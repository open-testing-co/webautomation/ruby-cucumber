# Ruby-Cucumber

Proyecto para automatizar google utilizando el Framework cucumber y el lenguaje Ruby.
By @handresc1127

## Prerequisitos

* Instalación de Ruby. https://www.ruby-lang.org/en/downloads/
* Abrir terminal en el directorio del proyecto 

## Instalación

* Ejecutar `bundle install` para instalar todas las dependencias
* Ejecutar `cucumber --init` para crear base del proyecto

# Ejecución
* Configurar la versión del driver dependiendo de la versión del navegador en `features\pages\PageGoogle.rb`
* Ejecutar `cucumber`
