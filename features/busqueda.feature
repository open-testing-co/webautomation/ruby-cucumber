Feature: Busqueda en google
 
  Scenario: Autocorreccion de busqueda y validacion de resultados
    Given abro un navegador "chrome"
    And voy a la url "https://www.google.com/"
    When escribo "pruebaz" en el elemento "caja de busqueda"
    And espero por el elemento "botón buscar"
    And doy clic en "botón buscar"
    Then el elemento "autocorrección" tiene el texto "pruebas"
    And doy clic en "autocorrección"
    And los subelementos de "resultados" son "<" a 6