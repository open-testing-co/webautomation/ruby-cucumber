class Objects
  @@objetos = Hash.new

  def self.setObject(key, value)
    @@objetos.store(formatObjectKey(key), value)
  end

  def self.getObject(key)
    return @@objetos[formatObjectKey(key)]
  end

  def self.formatObjectKey(str)
    accents = {
      ["á", "à", "â", "ä", "ã"] => "a",
      ["Ã", "Ä", "Â", "À"] => "A",
      ["é", "è", "ê", "ë"] => "e",
      ["Ë", "É", "È", "Ê"] => "E",
      ["í", "ì", "î", "ï"] => "i",
      ["Î", "Ì"] => "I",
      ["ó", "ò", "ô", "ö", "õ"] => "o",
      ["Õ", "Ö", "Ô", "Ò", "Ó"] => "O",
      ["ú", "ù", "û", "ü"] => "u",
      ["Ú", "Û", "Ù", "Ü"] => "U",
      ["ç"] => "c", ["Ç"] => "C",
      ["ñ"] => "n", ["Ñ"] => "N",
    }
    accents.each do |ac, rep|
      ac.each do |s|
        str = str.gsub(s, rep)
      end
    end
    return str.downcase.gsub(" ", "")
  end
end
