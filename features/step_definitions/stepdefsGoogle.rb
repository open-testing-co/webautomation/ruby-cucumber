require "selenium-webdriver"
require "webdriver"

Given(/^abro un navegador "([^"]*)"$/)  do |browser|
  @PageGoogle = PageGoogle.new(browser)
end

Given(/^voy a la url "([^"]*)"$/)  do |url|
  @PageGoogle.irA(url)
end

When(/^escribo "([^"]*)" en el elemento "([^"]*)"$/) do |texto, elemento|
  @PageGoogle.escribirTexto(texto, elemento)
end

Then(/^doy clic en "([^"]*)"$/) do |elemento|
  @PageGoogle.doyClic(elemento)
end

Then(/^el elemento "([^"]*)" tiene el texto "([^"]*)"$/) do |elemento, texto|
  @PageGoogle.tieneElTexto(texto, elemento)
end

Then(/^los subelementos de "([^"]*)" son "([^"]*)" a (\d+)$/)  do |elemento, comparacion, countElements|
  @PageGoogle.compararCantidaSubelementos(elemento, comparacion, countElements)
end

When(/^espero por el elemento "([^"]*)"$/) do |elemento|
  @PageGoogle.esperarElementoDisplayed(elemento)
end
