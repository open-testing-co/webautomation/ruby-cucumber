require_relative "../objects/Objects.rb"
require "rspec"
include RSpec::Matchers

class PageGoogle
  def initialize(browser)
    Selenium::WebDriver.logger.level = :error
    case browser
    when "firefox"
      options = Selenium::WebDriver::Firefox::Options.new
      Selenium::WebDriver::Firefox::Service.driver_path = "features/support/geckodriver.exe"
      @driver = Selenium::WebDriver.for :firefox
    else
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_argument("--start-maximized")
      Selenium::WebDriver::Chrome::Service.driver_path = "features/support/chromedriver_76.exe"
      @driver = Selenium::WebDriver.for :chrome, options: options
    end
  end

  def irA(url)
    @driver.navigate.to url
  end

  def escribirTexto(texto, elemento)
    @driver.find_element(Objects.getObject(elemento)).send_keys(texto)
  end

  def doyClic(elemento)
    @driver.find_element(Objects.getObject(elemento)).click
  end

  def tieneElTexto(texto, elemento)
    textoObtenido = @driver.find_element(Objects.getObject(elemento)).text
    expect(textoObtenido).to eq(texto)
  end

  def esperarElementoDisplayed(elemento)
    wait = Selenium::WebDriver::Wait.new(timeout: 15) # seconds
    wait.until { @driver.find_element(Objects.getObject(elemento)).displayed? }
  end

  def compararCantidaSubelementos(elemento, comparacion, countElements)
    padre = @driver.find_elements(Objects.getObject(elemento))
    expect(padre.length).to match(padre.length.to_s + comparacion + countElements)
    padre.length.should be >= 6
  end
end
